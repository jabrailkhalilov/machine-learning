import os
import subprocess
import shutil

# Запуск программы обучения
subprocess.run(['python', 'your_training_script.py'])

# Проверка наличия файла модели
if os.path.exists('trained_model.keras'):
    print("Model file found. Uploading to GitLab...")

    # Копирование файла модели в папку с данными
    shutil.copy('trained_model.keras', 'data/trained_model.keras')

    # Переход в директорию репозитория
    os.chdir('https://gitlab.com/jabrailkhalilov/machine-learning')

    # Добавление, коммит и пуш файла модели
    subprocess.run(['git', 'add', 'data/trained_model.keras'])
    subprocess.run(['git', 'commit', '-m', 'Add trained model'])
    subprocess.run(['git', 'push', 'origin', 'your_branch_name'])

    print("Model file uploaded to GitLab.")
else:
    print("Model file not found.")
